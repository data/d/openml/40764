# OpenML dataset: Car-test

https://www.openml.org/d/40764

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Source:

Creator: 

Marko Bohanec 

Donors: 

1. Marko Bohanec (marko.bohanec '@' ijs.si) 
2. Blaz Zupan (blaz.zupan '@' ijs.si)


Data Set Information:

Car Evaluation Database was derived from a simple hierarchical decision model originally developed for the demonstration of DEX, M. Bohanec, V. Rajkovic: Expert system for decision making. Sistemica 1(1), pp. 145-157, 1990.). The model evaluates cars according to the following concept structure: 

CAR car acceptability 
. PRICE overall price 
. . buying buying price 
. . maint price of the maintenance 
. TECH technical characteristics 
. . COMFORT comfort 
. . . doors number of doors 
. . . persons capacity in terms of persons to carry 
. . . lug_boot the size of luggage boot 
. . safety estimated safety of the car 

Input attributes are printed in lowercase. Besides the target concept (CAR), the model includes three intermediate concepts: PRICE, TECH, COMFORT. Every concept is in the original model related to its lower level descendants by a set of examples (for these examples sets see [Web Link]). 

The Car Evaluation Database contains examples with the structural information removed, i.e., directly relates CAR to the six input attributes: buying, maint, doors, persons, lug_boot, safety. 

Because of known underlying concept structure, this database may be particularly useful for testing constructive induction and structure discovery methods. 


Attribute Information:

Class Values: 

unacc, acc, good, vgood 

Attributes: 

buying: vhigh, high, med, low. 
maint: vhigh, high, med, low. 
doors: 2, 3, 4, 5more. 
persons: 2, 4, more. 
lug_boot: small, med, big. 
safety: low, med, high. 

#autoxgboost #autoweka

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40764) of an [OpenML dataset](https://www.openml.org/d/40764). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40764/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40764/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40764/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

